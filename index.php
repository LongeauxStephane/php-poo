<?php

//use Core\Router;
use App\Controllers\AnnonceController;
use App\Controllers\DetailAnnonceController;
use App\Controllers\PageController;
use App\Controllers\UserController;
use App\Controllers\SessionController;
use App\Controllers\FavorisController;
use App\Controllers\ReservationController;
use App\Controllers\AnnulationController;

use MiladRahimi\PhpRouter\Router;
use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
use MiladRahimi\PhpRouter\Exceptions\InvalidControllerException;
use Zend\Diactoros\Response\HtmlResponse;




// Paramètres Base de données
define( 'DB_HOST', 'localhost' );
define( 'DB_NAME', 'airbnb2' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );

// Paramètres PDO
define( 'PDO_ENGINE', 'mysql' );
define( 'PDO_OPTIONS', [
	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

// Chemins de fichiers
define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname(__FILE__) . DS );



//Style

spl_autoload_register();
session_start();

require 'vendor' . DS . 'autoload.php';

//
//if ( !is_array($_SESSION['favoris']) || !isset($_SESSION['favoris']) ) {
//    $_SESSION['favoris'] = [];
//}




// Initialisation du routeur
//$router = Router::get();
//$router->controllers_namespace = '\App\Controllers';

$router = new Router();
$router->get('/', PageController::class . '@index')

// Annonces
->get('/detail-annonce', DetailAnnonceController ::class . '@index')
->post('/detail-annonce', ReservationController ::class . '@reservation')
->get('/annonce', AnnonceController::class . '@index')
->get('/mes-annonces', AnnonceController::class . '@mesAnnonces')
->post('/creation-annonce', AnnonceController::class . '@creationAnnonce')
->get('/creation-annonce', AnnonceController::class . '@creationAnnoncePage')
    ->get('/favoris', FavorisController::class . '@mesFavoris')

->get('/connexion', UserController::class . '@index')
->post('/connexion', UserController::class . '@authentication')

->get('/inscription', UserController::class . '@inscriptionPage')
->post('/inscription', UserController::class . '@inscription')

->get('/deconnexion', SessionController::class . '@sessionDestroy')



->get('/reservation', ReservationController::class . '@reservationPage')
->post('/reservation', ReservationController::class . '@reservation')

->get('/annulation', AnnulationController::class . '@AnnulationReservation')


->post('/mes-reservation', ReservationController::class . '@AnnonceurReservation')
->get('/mes-reservation', ReservationController::class . '@AnnonceurReservation')

;

try {
    $router->dispatch();
} catch (RouteNotFoundException $e) {
    var_dump($e);
    $router->getPublisher()->publish(new HtmlResponse('Not found.', 404));
} catch (Throwable $e) {
    // Log and report...
    var_dump($e);
    $router->getPublisher()->publish(new HtmlResponse('Internal error.', 500));

} catch (InvalidControllerException $e) {
    $router->getPublisher()->publish(new HtmlResponse('InvalidControllerException.', 500));
}



// Déclaration des URL de l'application
//$router->registerRoute( '/', 'PageController', 'index' );
//$router->registerRoute( '/users', 'UserController', 'index' );
//$router->registerRoute( '/categories', 'CategoryController', 'index' );
//$router->registerRoute( '/lol', 'CategoryController', 'lol' );
//$router->registerRoute( '/annonce', 'AnnonceController', 'index' );
//$router->registerRoute( '/connexion', 'UserController', 'index' );
//$router->registerRoute( '/detail-annonce', 'AnnonceController', 'index' );

// Démarrage du routeur
/*$router->start();*/
