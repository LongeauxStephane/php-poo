-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.21 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour airbnb2
CREATE DATABASE IF NOT EXISTS `airbnb2` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `airbnb2`;

-- Listage de la structure de la table airbnb2. annonce
CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titre` varchar(250) DEFAULT NULL,
  `chambre_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Listage des données de la table airbnb2.annonce : ~0 rows (environ)
/*!40000 ALTER TABLE `annonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `annonce` ENABLE KEYS */;

-- Listage de la structure de la table airbnb2. chambre
CREATE TABLE IF NOT EXISTS `chambre` (
  `id` int NOT NULL AUTO_INCREMENT,
  `annonceur_id` int DEFAULT NULL,
  `titre` varchar(50) DEFAULT NULL,
  `prix` int DEFAULT NULL,
  `description_courte` text,
  `description` text,
  `taille` int DEFAULT NULL,
  `img` varchar(250) DEFAULT NULL,
  `equipement` int DEFAULT NULL,
  `reserv_etat` tinyint(1) DEFAULT NULL,
  `reserv_date` date DEFAULT NULL,
  `chb_type` int DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `nb_chb` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Listage des données de la table airbnb2.chambre : ~15 rows (environ)
/*!40000 ALTER TABLE `chambre` DISABLE KEYS */;
INSERT INTO `chambre` (`id`, `annonceur_id`, `titre`, `prix`, `description_courte`, `description`, `taille`, `img`, `equipement`, `reserv_etat`, `reserv_date`, `chb_type`, `pays`, `ville`, `nb_chb`) VALUES
	(1, 1, 'Annonce 1', 177, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur turpis et nisl fringilla, at maximus ligula cursus. Nunc vel porta elit, eget iaculis mauris. Etiam pretium metus eget nulla sollicitudin pretium. Pellentesque bibendum erat ac vehicula iaculis. Etiam cursus mi sem, eget euismod dui bibendum eu. Proin pretium, tortor a efficitur finibus, mi enim porttitor quam, non convallis turpis est et nulla. Etiam risus libero, porta vel laoreet iaculis, vulputate vitae odio. Vivamus ullamcorper metus non vestibulum faucibus.', NULL, NULL, 2, NULL, NULL, 1, 'France', 'Narbonne', NULL),
	(2, 2, 'Annonce 2', 265, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur turpis et nisl fringilla, at maximus ligula cursus. Nunc vel porta elit, eget iaculis mauris. Etiam pretium metus eget nulla sollicitudin pretium. Pellentesque bibendum erat ac vehicula iaculis. Etiam cursus mi sem, eget euismod dui bibendum eu. Proin pretium, tortor a efficitur finibus, mi enim porttitor quam, non convallis turpis est et nulla. Etiam risus libero, porta vel laoreet iaculis, vulputate vitae odio. Vivamus ullamcorper metus non vestibulum faucibus.', NULL, NULL, 2, NULL, NULL, 2, 'Espagne', 'Barcelone', NULL),
	(3, 3, 'Annonce 3', 65, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur turpis et nisl fringilla, at maximus ligula cursus. Nunc vel porta elit, eget iaculis mauris. Etiam pretium metus eget nulla sollicitudin pretium. Pellentesque bibendum erat ac vehicula iaculis. Etiam cursus mi sem, eget euismod dui bibendum eu. Proin pretium, tortor a efficitur finibus, mi enim porttitor quam, non convallis turpis est et nulla. Etiam risus libero, porta vel laoreet iaculis, vulputate vitae odio. Vivamus ullamcorper metus non vestibulum faucibus.', NULL, NULL, 3, NULL, NULL, 3, 'Espagne', 'Madrid', NULL),
	(4, 2, 'Annonce 4', 105, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur turpis et nisl fringilla, at maximus ligula cursus. Nunc vel porta elit, eget iaculis mauris. Etiam pretium metus eget nulla sollicitudin pretium. Pellentesque bibendum erat ac vehicula iaculis. Etiam cursus mi sem, eget euismod dui bibendum eu. Proin pretium, tortor a efficitur finibus, mi enim porttitor quam, non convallis turpis est et nulla. Etiam risus libero, porta vel laoreet iaculis, vulputate vitae odio. Vivamus ullamcorper metus non vestibulum faucibus.', NULL, NULL, 1, NULL, NULL, 1, 'France', 'Corse', NULL),
	(5, 2, 'Annonce 5', 165, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur turpis et nisl fringilla, at maximus ligula cursus. Nunc vel porta elit, eget iaculis mauris. Etiam pretium metus eget nulla sollicitudin pretium. Pellentesque bibendum erat ac vehicula iaculis. Etiam cursus mi sem, eget euismod dui bibendum eu. Proin pretium, tortor a efficitur finibus, mi enim porttitor quam, non convallis turpis est et nulla. Etiam risus libero, porta vel laoreet iaculis, vulputate vitae odio. Vivamus ullamcorper metus non vestibulum faucibus.', NULL, NULL, 2, NULL, NULL, 2, 'UK', 'Londres', NULL),
	(6, 3, 'Annonce 6', 1200, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur turpis et nisl fringilla, at maximus ligula cursus. Nunc vel porta elit, eget iaculis mauris. Etiam pretium metus eget nulla sollicitudin pretium. Pellentesque bibendum erat ac vehicula iaculis. Etiam cursus mi sem, eget euismod dui bibendum eu. Proin pretium, tortor a efficitur finibus, mi enim porttitor quam, non convallis turpis est et nulla. Etiam risus libero, porta vel laoreet iaculis, vulputate vitae odio. Vivamus ullamcorper metus non vestibulum faucibus.', NULL, NULL, 3, NULL, NULL, 3, 'UK', 'Manchester', NULL),
	(7, 2, 'Le chat', 100, 'Miaou', 'Mioua', 12, NULL, 1, NULL, NULL, 3, 'USA', 'San Fransisco', NULL),
	(19, 2, 'Test', 2, 'Test', 'Test', 1, NULL, NULL, NULL, NULL, NULL, 'France', 'Narbonne', NULL),
	(20, 2, 'Yami', 2, 'jhjh', 'hjhj', 1, NULL, 2, NULL, NULL, NULL, 'France', 'Narbonne', NULL),
	(21, 2, 'Yami', 2, 'jhjh', 'hjhj', 1, NULL, 2, NULL, NULL, NULL, 'France', 'Narbonne', NULL),
	(22, 2, 'Yami', 2, 'jhjh', 'hjhj', 1, NULL, 2, NULL, NULL, NULL, 'France', 'Narbonne', NULL),
	(23, 2, 'La voilà', 10, 'ytutyu', 'tyutyu', 3, NULL, 3, NULL, NULL, 3, 'France', 'Narbonne', NULL),
	(24, 2, 'Le chien', 5, 'jkljk', 'jkllj', 5, NULL, 2, NULL, NULL, 2, 'France', 'Narbonne', NULL),
	(25, 2, 'Avec nb chambre', 10, 'Avec nb chambre', 'Avec nb chambre', 5, NULL, 2, NULL, NULL, 2, 'France', 'Narbonne', 7),
	(26, 2, 'A supprimer', 1, 'A supprimer', 'A supprimer', 1, NULL, 1, NULL, NULL, 1, '1', '1', 2);
/*!40000 ALTER TABLE `chambre` ENABLE KEYS */;

-- Listage de la structure de la table airbnb2. equipement
CREATE TABLE IF NOT EXISTS `equipement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Listage des données de la table airbnb2.equipement : ~2 rows (environ)
/*!40000 ALTER TABLE `equipement` DISABLE KEYS */;
INSERT INTO `equipement` (`id`, `label`) VALUES
	(1, 'Frigot'),
	(2, 'Télévision'),
	(3, 'Mini-bar');
/*!40000 ALTER TABLE `equipement` ENABLE KEYS */;

-- Listage de la structure de la table airbnb2. favoris
CREATE TABLE IF NOT EXISTS `favoris` (
  `id` int NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int DEFAULT '0',
  `chambre_id` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Listage des données de la table airbnb2.favoris : ~0 rows (environ)
/*!40000 ALTER TABLE `favoris` DISABLE KEYS */;
INSERT INTO `favoris` (`id`, `utilisateur_id`, `chambre_id`) VALUES
	(1, 1, 2);
/*!40000 ALTER TABLE `favoris` ENABLE KEYS */;

-- Listage de la structure de la table airbnb2. liaison_equipement
CREATE TABLE IF NOT EXISTS `liaison_equipement` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chambre_id` int DEFAULT NULL,
  `label_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Listage des données de la table airbnb2.liaison_equipement : ~0 rows (environ)
/*!40000 ALTER TABLE `liaison_equipement` DISABLE KEYS */;
INSERT INTO `liaison_equipement` (`id`, `chambre_id`, `label_id`) VALUES
	(1, 1, 2);
/*!40000 ALTER TABLE `liaison_equipement` ENABLE KEYS */;

-- Listage de la structure de la table airbnb2. reservation
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chambre_id` int DEFAULT NULL,
  `utilisateur_id` int DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `annonceur_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Listage des données de la table airbnb2.reservation : ~3 rows (environ)
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` (`id`, `chambre_id`, `utilisateur_id`, `date_debut`, `date_fin`, `annonceur_id`) VALUES
	(3, 1, 1, NULL, NULL, NULL),
	(4, 4, 2, NULL, NULL, NULL),
	(25, 1, 1, NULL, NULL, 2),
	(27, 4, 1, NULL, NULL, 2);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;

-- Listage de la structure de la table airbnb2. utilisateur
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  `prenom` varchar(250) DEFAULT NULL,
  `login` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `adresse` text,
  `telephone` decimal(10,0) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `chambres` int DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Listage des données de la table airbnb2.utilisateur : ~2 rows (environ)
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `login`, `password`, `adresse`, `telephone`, `email`, `chambres`, `role`) VALUES
	(1, 'Jean', 'Patrick', 'jean', 'jean', NULL, NULL, NULL, NULL, 'utilisateur'),
	(2, NULL, NULL, 'admin', 'admin', NULL, NULL, NULL, NULL, 'annonceur'),
	(7, 'Longeaux', 'STEPHANE', 'Steph', 'steph', NULL, NULL, NULL, NULL, 'annonceur');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
