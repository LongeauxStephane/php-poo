<?php


namespace App\Controllers;


use Core\Controller;
use Core\View;
use http\Header;
use Zend\Diactoros\ServerRequest;

class AnnulationController extends Controller
{

    public function AnnulationReservation(ServerRequest $request)
    {
        $view = new View( 'annulation' );

        $id = $_SESSION['id'];

        $post = $request->getParsedBody();

        $this->rm->getAnnulationRepo()->annulation();

        $view_data = [
            'mes_reservations'	=>   $this->rm->getReservationRepo()->ReadAnnonceurReservation($id)
        ];


        $view->render($view_data);

        header('Location: /reservation');


    }






}