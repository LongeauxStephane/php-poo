<?php


namespace App\Controllers;

use Core\Controller;
use Core\View;


class SessionController  extends Controller
{

    public function sessionDestroy(): void
    {
        $view = new View( 'sessionDestroy' );
        $view->render();
    }

}