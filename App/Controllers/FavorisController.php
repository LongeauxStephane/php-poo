<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;

class FavorisController extends Controller
{
    public function mesFavoris(): void
    {
        $view = new View( 'favoris' );

        $id = $_SESSION['id'];

        $view_data = [
            'html_title'	=> 'Mon Super site - accueil',
            'page_title'		=> 'Mes annonces',
            'mes_favoris'	=> $this->rm->getReservationRepo()->readReservation($id)
        ];


        $view->render($view_data );

    }



}