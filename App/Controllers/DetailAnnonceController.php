<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use App\Models\User;



class DetailAnnonceController extends Controller
{
//    public function index(): void
//    {
//        $view = new View('detail_annonce');
//        $id = $_GET['id'];
//
//
//        $view_data = [
//            'html_title' => 'Mon Super site - accueil',
//            'page_title' => 'Annonces',
//            'annonce' => $this->rm->getAnnonceRepo()->findById($id)
//        ];
//
//        $view->render($view_data);
//    }

    public function index(): void
    {
        $view = new View('detail_annonce');
        $id = $_GET['id'];


        $view_data = [
            'html_title' => 'Mon Super site - accueil',
            'page_title' => 'Annonces',
            'annonce' => $this->rm->getAnnonceRepo()->findById($id),
            'equipement' => $this->rm->getAnnonceRepo()->equipementAnnonce($id)
        ];

        $view->render($view_data);
    }

}