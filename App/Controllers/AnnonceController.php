<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use Zend\Diactoros\ServerRequest;


class AnnonceController extends Controller
{
    public function index(): void
    {
        $view = new View( 'annonce' );

        $view_data = [
            'html_title'	=> 'Mon Super site - accueil',
			'page_title'		=> 'Annonces',
			'annonces'	=> $this->rm->getAnnonceRepo()->findAll()
        ];

        $view->render( $view_data );
    }


    public function mesAnnonces(): void
    {
        $view = new View( 'mes_annonces' );

        $id = $_SESSION['id'];

        $view_data = [
            'html_title'	=> 'Mon Super site - accueil',
            'page_title'		=> 'Mes annonces',
            'mes_annonces'	=> $this->rm->getAnnonceRepo()->findAnnonceByID($id)
        ];


        $view->render($view_data );

    }

    public function creationAnnoncePage(): void
    {
        $view = new View( 'creation_annonce' );
        $view->render();


    }

    public function creationAnnonce(ServerRequest $request): void
    {
        $post = $request->getParsedBody();

        $this->rm->getAnnonceRepo()->createAnnonce();

        header('Location: /mes-annonces');

    }



}