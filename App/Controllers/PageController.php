<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;

class PageController extends Controller
{
	public function index(): void
	{
		$view = new View( 'home' );

		$view_data = [
            'latest_annonces'	=> $this->rm->getAnnonceRepo()->findAll()
		];

		$view->render( $view_data );
	}
}