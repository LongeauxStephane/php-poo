<?php

namespace App\Controllers;

use Core\Controller;
use Core\View;
use Zend\Diactoros\ServerRequest;
use \PDO;

class UserController extends Controller
{
	public function index(): void
	{
		$view = new View( 'connexion' );

		$view_data = [
			'html_title' => 'Liste des utilisateurs',
			'html_h1' => 'Les utilisateurs',
			'users' => $this->rm->getUserRepo()->findAll()
		];

		$view->render( $view_data );
	}


	public function authentication()
    {
        $login = $_POST['login'];
        $password = $_POST['password'];

        $user = $this->rm->getUserRepo()->findByLogin($login);

        if ( $login == $user->login && $password == $user->password) {
            header('Location: /');

            $_SESSION['Status_cnx'] = true;
            $_SESSION['role'] = $user->role;
            $_SESSION['login'] = $user->login;
            $_SESSION['id'] = $user->id;

        } else {
            header('Location: /connexion');
            $_SESSION['Status_cnx'] = false;
        }

    }

    public function inscriptionPage()
    {
        $view = new View( 'inscription' );
        $view->render();
    }

    public function inscription(ServerRequest $request): void
    {
        $post = $request->getParsedBody();

        $this->rm->getUserRepo()->inscription();

        header('Location: /inscription');

    }

}