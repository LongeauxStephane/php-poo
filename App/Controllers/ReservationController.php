<?php


namespace App\Controllers;


use Core\Controller;
use Core\View;
use http\Header;
use Zend\Diactoros\ServerRequest;

class ReservationController extends Controller
{

    public function reservationPage(): void
    {
        $view = new View( 'reservations' );

        $id = $_SESSION['id'];

        $view_data = [
            'html_title'	=> 'Mon Super site - accueil',
            'page_title'		=> 'Mes annonces',
            'mes_reservations'	=> $this->rm->getReservationRepo()->readReservation($id)
        ];

        $view->render($view_data );

    }

    public function reservation(ServerRequest $request)
    {
        $id = $_SESSION['id'];

        $post = $request->getParsedBody();
        $this->rm->getReservationRepo()->Readreservation($id);

        $this->rm->getReservationRepo()->reservation();

        header( 'Location: /reservation');
    }

    public function AnnonceurReservation(ServerRequest $request)
    {
        $view = new View( 'mes_reservations' );


        $id = $_SESSION['id'];

        $post = $request->getParsedBody();

        $view_data = [
            'mes_reservations'	=>   $this->rm->getReservationRepo()->ReadAnnonceurReservation($id)
        ];

        $view->render($view_data);

    }

    public function AnnulationReservation(ServerRequest $request)
    {
        $view = new View( 'reservations' );

        $id = $_SESSION['id'];

        $post = $request->getParsedBody();

       $this->rm->getReservationRepo()->annulation();

        $view_data = [
            'mes_reservations'	=>   $this->rm->getReservationRepo()->ReadAnnonceurReservation($id)
        ];


        $view->render($view_data);

       header('Location: /reservation');



    }






}