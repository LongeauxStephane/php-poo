<?php
include 'includes/header.php';
?>

<h1>Mes favoris : </h1>

<div class="container">
    <div class="box box-favoris cards-layout">
        <ul>
            <?php foreach($mes_favoris as $favoris): ?>
                <div class="col-lg-4 col-md-4 col-4">
                    <form action="" method="GET">
                        <div class="card-chb">
                            <a href="#" class="link-ann">
                                <div class="cont-img">
                                    <img src="\Uploads\cat.jpg" alt="">
                                </div>
                                <div class="cont-content card-content">
                                    <h4><?= $favoris['titre'] ?></h4>
                                    <span class="tag-prix"><?=  $favoris['prix'] ?>€</span>
                                    <button class="btn-fav" name="favoris" value="<?=  $favoris['id'] ?>"><i class="far fa-heart"></i></button>
                                    <span class="bloc bloc-description">
                                           <?=  $favoris['description_courte'] ?>
                                        </span>
                                    <button type="submit" class="btn btn-detail" name="reservation" value="<?=  $favoris['id'] ?>">Voir</button>
                                </div>
                            </a>
                        </div>
                    </form>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php
include 'includes/footer.php'
?>


