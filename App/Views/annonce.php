<?php
include 'includes/header.php';
include 'includes/redirect.php';
require_once 'includes/upload.php';
?>


<h1><?= $page_title ?></h1>

<div class="container">
    <div class="box box-annonces cards-layout">
            <ul>
                <?php foreach($annonces as $annonce): ?>
                    <div class="col-lg-4 col-md-4 col-4">
                        <form action="" method="GET">
                            <div class="card-chb">
                                <a href="#" class="link-ann">
                                    <div class="cont-img">
                                        <img src="\Uploads\cat.jpg" alt="">
                                    </div>
                                    <div class="cont-content card-content">
                                        <h4><?= $annonce->titre ?></h4>
                                        <span class="tag-prix"><?= $annonce->prix ?>€</span>
                                        <button class="btn-fav" name="favoris" value="<?= $annonce->id ?>"><i class="far fa-heart"></i></button>
                                        <span class="bloc bloc-description">
                                           <?= $annonce->description_courte ?>
                                        </span>
                                        <a href="<?= '/detail-annonce?id=' . $annonce->id ?>" class="btn btn-detail" name="reservation" value="<?= $annonce->id ?>">Voir</a>
                                    </div>

                                </a>
                            </div>
                        </form>
                    </div>
                <?php endforeach; ?>
            </ul>
        <?php


//
//            if ( isset ( $_GET['favoris'])) {
//                array_push( $_SESSION['favoris'], $_GET['favoris']) ;
//                $_SESSION['favoris'] = array_unique($_SESSION['favoris']);
//            }


//
//           var_dump($_SESSION['favoris']);


        ?>
        </div><!-- Fin box -->
</div><!-- Fin container -->





<!--
<form action="" method="POST" name="form-img-uploader" enctype="multipart/form-data">
    <input type="file" name="file">
    <button type="submit" id="img-upload" name="img-upload" >Envoyer</button>
</form>
-->




<?php
include 'includes/footer.php'
?>





