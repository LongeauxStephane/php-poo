<?php
include 'includes/header.php';
require_once 'includes/upload.php';
?>


<div class="container">
    <div class="box box-detail-annonces cards-layout">
        <div class="detail-header">
            <h1><?= $annonce->titre; ?></h1>
            <span class="tag-prix"><?= $annonce->prix ?>€</span>
        </div>
        <div class="cont-img detail-img">
            <img src="\Uploads\cat.jpg" alt="">
        </div>
        <div class="detail-content">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-6">
                    <div class="box-equipement">
                        <?php
                        switch($annonce->chb_type) {
                            case 1:
                                ?>
                                    <div class='row'>
                                        <div class="col-lg-3 col-md-3 col-3">
                                            <span class='data-chb'>Chambre entière</span>
                                        </div>
                                    </div>
                                <?php
                                break;

                            case 2:
                                ?>
                                    <div class='row'>
                                        <div class="col-lg-3 col-md-3 col-3">
                                            <span class='data-chb'>Chambre privée</span>
                                        </div>
                                    </div>
                                <?php
                                break;

                            case 3:
                                ?>
                                <div class='row'>
                                    <div class="col-lg-3 col-md-3 col-3">
                                        <span class='data-chb'>Chambre privée</span>
                                    </div>
                                </div>
                                <?php
                                break;

                            default:
                                break;
                        }

                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-6">
                    <ul>
                        <?php
                            echo '<li>' . $equipement['label'] . '</li>';
                        ?>
                    </ul>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="detail-description">
                        <?= $annonce->description ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="adresse">
                        <ul>
                            <li>Pays: <?= $annonce->pays ?></li>
                            <li>Ville: <?= $annonce->ville ?></li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="cont-resrv">
                        <form action="/reservation" method="POST">
                            <button type="submit" name="reservation" class="btn btn-brand" value="<?= $annonce->id ?>">Reserver</button>
                            <input style="visibility: hidden; display: none;" name="annonceur" value="<?= $annonce->annonceur_id ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
include 'includes/footer.php'
?>

