<?php
include 'includes/header.php';
include 'includes/redirect.php';
require_once 'includes/upload.php';
?>

<h1>Creation d'annonce</h1>


<div class="container">
        <div class="box box-annonces box-creation-annonce cards-layout">
            <div class="row">
                <form action="/creation-annonce" method="POST" name="form-creation-annonce" class="form-creation-annonce" enctype="multipart/form-data">

                    <div class="cont-box">
                        <h5 for="titre-annonce">Titre</h5>
                        <input type="text" name="titre-annonce" id="">
                    </div>

                    <div class="cont-box">
                        <h5 for="description-courte-annonce">Description Courte</h5>
                        <textarea name="description-courte-annonce" id="" cols="30" rows="10"></textarea>
                    </div>

                    <div class="cont-box">
                        <h5 for="description-annonce">Description</h5>
                        <textarea name="description-annonce" id="" cols="30" rows="10"></textarea>
                    </div>

                    <div class="cont-box cont-box-img-upload">
                        <input type="file" name="file">
                        <button type="submit" id="img-upload" name="img-upload" >Envoyer</button>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="cont-box">
                                <label for="taille-annonce">Taille</label>
                                <input type="number" name="taille-annonce" id="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="cont-box">
                                <label for="prix-annonce">Prix</label>
                                <input type="number" name="prix-annonce" id="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="cont-box">
                                <label for="pays-annonce">Pays</label>
                                <input type="text" name="pays-annonce" id="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="cont-box">
                                <label for="ville-annonce">Ville</label>
                                <input type="text" name="ville-annonce" id="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <div class="cont-box">
                                <label for="nb-chb-annonce">Nombre de chambre</label>
                                <input type="number" name="nb-chb-annonce" id="">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Equipement</h5>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="">Television</label>
                                <input type="checkbox" name="equipement[]"  value="1">
                            </div>
                            <div class="col-lg-4">
                                <label for="">Frigot</label>
                                <input type="checkbox" name="equipement[]"  value="2">
                            </div>
                            <div class="col-lg-4">
                                <label for="">Mini-bar</label>
                                <input type="checkbox" name="equipement[]" value="3">
                            </div>
                        </div>
                     </div>



                    <div class="cont-box cont-box-equipement">
                        <h4 for="">Type de chambre</h4>
                        <div class="box-chb-type">

                            <input type="checkbox" name="type_chb[]" value="1">
                            <label for="">Individuel</label>

                            <input type="checkbox" name="type_chb[]" value="2">
                            <label for="">Partagée</label>

                            <input type="checkbox" name="type_chb[]" value="3">
                            <label for="">Entière</label>

                        </div>
                    </div>
                    <button type="submit" class="btn-create" name="create">Créer</button>
                </form>
            </div>
        </div>
</div>



<?php
include 'includes/footer.php'
?>


