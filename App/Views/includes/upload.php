<?php

/***************************************************************************/
/****************************** IMAGES *************************************/
/***************************************************************************/

if( isset( $_POST['img-upload']) ) {
    $file = $_FILES['file'];

    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileError = $_FILES['file']['error'];
    $fileType = $_FILES['file']['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'png');

    if ( in_array( $fileActualExt, $allowed) ) {
        if ($fileError === 0) {
            if ($fileSize < 500000) {
                $fileNameNew = uniqid('', true) . '.' . $fileActualExt;
                $fileDestination = 'uploads/' . $fileNameNew;
                move_uploaded_file($fileTmpName , $fileDestination);
                echo 'Image uploadée';
            } else {
                echo 'Votre fichier est trop volumineux, moins de 5mb attendu';
            }
        } else {
            echo 'Il y a eu une erreur en chargeant votre image';
        }
    } else {
        echo 'Vous ne pouvez pas charger que des images en jpg ou png';
    }
}

/***************************************************************************/
/****************************** ANNONCE *************************************/
/***************************************************************************/



