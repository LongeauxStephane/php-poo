
    <div class="box box-annonces cards-layout">
        <?php if( count($latest_annonces) > 0 ): ?>
            <ul>
                <?php $i = 0 ?>
                <?php foreach($latest_annonces as $i => $annonce): ?>
                    <?php
                        if ($i >= 3) {
                            break;
                        }
                    ?>
                    <div class="col-lg-4 col-md-4 col-4">
                        <div class="card-chb">
                            <a href="#" class="link-ann">
                                <div class="cont-img">
                                    <img src="\Uploads\cat.jpg" alt="">
                                </div>
                                <div class="cont-content card-content">
                                    <h4><?= $annonce->titre ?></h4>
                                    <span class="tag-prix"><?= $annonce->prix ?>€</span>
                                    <button type="submit" class="btn-fav"><i class="far fa-heart"></i></button>
                                    <span class="bloc bloc-description">
                                       <?= $annonce->description_courte ?>
                                    </span>
                                    <a href="<?= '/detail-annonce?id=' . $annonce->id ?>" class="btn btn-detail" name="reservation" value="<?= $annonce->id ?>">Voir</a>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        </div><!-- Fin box -->


