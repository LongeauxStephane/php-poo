<!--<!DOCTYPE html>-->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/App/Views/assets/css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,400;1,700&display=swap" rel="stylesheet">

    <title>AirBnB </title>
</head>

<body>

<div class="container-fluid"><!-- Main container -->


        <nav class="navbar main-bar">
            <a class="brand" href="/">airbnb</a>
                <ul class="menu main-menu">
                    <li><a href="/annonce">Annonces</a></li>
<!--                    --><?php //var_dump( $_SESSION['role']); ?>
<!--                    --><?php //var_dump( $_SESSION['login']); ?>
<!--                    --><?php //var_dump( $_SESSION['user']); ?>

<!--                    --><?php //var_dump( $_POST['login']); ?>
                    <?php
                        if ( isset( $_SESSION['role'] ) && $_SESSION['role'] == 'utilisateur') {
                            echo '<li><a href="/favoris">Mes favoris</a></li>';
                            echo '<li><a href="/reservation">Mes reservations</a></li>';
                        }
                        else if ( isset( $_SESSION['role'] ) && $_SESSION['role'] == 'annonceur') {
                            echo '<li><a href="/mes-annonces">Mes annonces</a></li>';
                            echo '<li><a href="/mes-reservation">Mes reservations</a></li>';
                        }
                    ?>
                </ul>
            <?php
                if ( isset( $_SESSION['Status_cnx'] ) && $_SESSION['Status_cnx'] ) {
                   echo '<a href="/deconnexion" class="btn-cnx">Deconnexion</a>';
                } else {
                   echo '<a href="connexion" class="btn-cnx">Connexion</a>';
                }
            ?>
<!--            <a href="connexion" class="btn-cnx">Connexion</a>-->
        </nav>

