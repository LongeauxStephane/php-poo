<?php
include 'includes/header.php';
?>

<h1>Mes annonces : </h1>

<div class="container">
    <div class="box box-annonces cards-layout">
        <div class="row">
            <a href="/creation-annonce" class="btn btn-create">Creer une annonce</a>
        </div>
        <ul>
            <?php foreach($mes_annonces as $annonce): ?>
                <div class="col-lg-4 col-md-4 col-4">
                    <form action="" method="GET">
                        <div class="card-chb">
                            <a href="#" class="link-ann">
                                <div class="cont-img">
                                    <img src="\Uploads\cat.jpg" alt="">
                                </div>
                                <div class="cont-content card-content">
                                    <h4><?= $annonce->titre ?></h4>
                                    <span class="tag-prix"><?=  $annonce->prix ?>€</span>
                                    <button class="btn-fav" name="favoris" value="<?=  $annonce->id ?>"><i class="far fa-heart"></i></button>
                                    <span class="bloc bloc-description">
                                           <?=  $annonce->description_courte ?>
                                        </span>
                                    <a href="<?= '/detail-annonce?id=' . $annonce->id ?>" class="btn btn-detail" name="reservation" value="<?= $annonce->id ?>">Voir</a>
                                </div>
                            </a>
                        </div>
                    </form>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php
include 'includes/footer.php'
?>


