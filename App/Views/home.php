<?php
include 'includes/header.php';
require_once 'includes/upload.php';
?>


<title><?php echo $html_title ?></title>

<div class="cont-diaporama">
    <div class="container box-presentation">
        <h2>Le dépaysement à deux pas de chez vous</h2>
        <span>
            Trouvez votre nouveau chez vous. Découvrez des logements à proximité pour vous installer, travailler, ou tout simplement vous détendre.
        </span>
    </div>
</div>

<div class="container cont-nogrow">
    <div class="box box-latest-annonces">
        <?php include 'includes/latest_annonces.php'; ?>
    </div>
</div>



<?php
include 'includes/footer.php'
?>
