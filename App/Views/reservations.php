<?php
include 'includes/header.php';
?>


<h1>Mes réservations</h1>

<div class="container">
    <div class="box box-favoris cards-layout">
        <ul>
            <?php foreach($mes_reservations as $reservation): ?>
                <div class="col-lg-4 col-md-4 col-4">
                    <form action="/annulation" method="GET">
                        <div class="card-chb">
                            <a href="#" class="link-ann">
                                <div class="cont-img">
                                    <img src="\Uploads\cat.jpg" alt="">
                                </div>
                                <div class="cont-content card-content">
                                    <h4><?= $reservation['titre'] ?></h4>
                                    <span class="tag-prix"><?=  $reservation['prix'] ?>€</span>
                                    <button class="btn-fav" name="favoris" value="<?=  $reservation['id'] ?>"><i class="far fa-heart"></i></button>
                                    <span class="bloc bloc-description">
                                           <?=  $reservation['description_courte'] ?>
                                        </span>
                                    <div class="box-btn">
                                        <a href="<?= '/detail-annonce?id=' . $reservation['id'] ?>" class="btn btn-detail" name="reservation" value="<?= $reservation['id'] ?>">Voir</a>
                                        <button type="submit" class="btn btn-brand" name="annuler" value="<?= $reservation['id'] ?>">Annuler</button>
                                    </div>

                                </div>
                            </a>
                        </div>
                    </form>
                </div>
            <?php endforeach; ?>
        </ul>
    </div>

</div>


<?php
include 'includes/footer.php'
?>


