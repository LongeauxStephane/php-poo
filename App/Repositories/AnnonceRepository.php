<?php


namespace App\Repositories;

use App\Models\Annonce;
use App\Models\Equipement;
use Core\IModel;
use Core\Repository;
use \PDO;



class AnnonceRepository extends Repository
{

    public function getTable(): string
    {
        return 'chambre';
    }
    // CRUD
    // Read: Toute la liste
    public function findAll(): array
    {
        return $this->readAll( Annonce::class );
    }

    // Read: Une annonce par son ID
    public function findById( int $id ): ?Annonce
    {
        return $this->readById( $id, Annonce::class );
    }

    // Read: Une annonce par l'ID de son createur (user)
    public function findAnnonceByID( int $id ): ?array
    {
        return $this->readAnnoncesByUserId( $id, Annonce::class );
    }

    public function equipementAnnonce( int $id ): ?array
    {

        $query = sprintf(
            "SELECT * FROM chambre c JOIN equipement e ON c.equipement = e.id
                    WHERE c.id=:id",
            $this->getTable()
        );

        $sth = $this->db_cnx->prepare($query);
        if (!$sth) {
            return null;
        }
        // $sth->execute( [ 'id' => $id ]);

        // Attachement d'un paramètre avec précision de type
        $sth->bindValue( 'id', $id, PDO::PARAM_INT );

        // Exécution de la requête préparée
        $sth->execute();

//        $sth->debugDumpParams();

        // En cas d'erreur du serveur SQL on retourne null
        if ($sth->errorCode() !== PDO::ERR_NONE) {
            return null;
        }

//        $annonce = [];
//    while ($row = $sth->fetch() )
//    {
//        $annonce[] = new Equipement($row);
//    }
        $row = $sth->fetch();
        if (!$row) {
            return null;
        }


        return $row;
    }


    public function createAnnonce( ): ?Annonce
    {
        if (isset($_POST['create'])) {
            $titre = $_POST['titre-annonce'];
            $description = $_POST['description-annonce'];
            $description_courte = $_POST['description-courte-annonce'];
            $taille = $_POST['taille-annonce'];
            $prix = $_POST['prix-annonce'];
            $pays = $_POST['pays-annonce'];
            $ville = $_POST['ville-annonce'];
            $nb_chambre = $_POST['nb-chb-annonce'];



            $equipement = $_POST['equipement'];
            $equipement = implode('', $equipement);

            $type_chb = $_POST['type_chb'];
            $type_chb = implode('', $type_chb);

            $annonceur_id = $_SESSION['id'];

            $query = sprintf(
                "INSERT INTO chambre (
                                    `titre`,
                                    `description_courte`,
                                    `description`,
                                    `taille`,
                                    `prix`, 
                                    `annonceur_id`,
                                     `pays`,
                                      `ville`,
                                      `equipement`,
                                      `chb_type`,
                                      `nb_chb`
                                  
                                      )
                        VALUES (
                            '$titre',
                             '$description_courte',
                             '$description',
                              '$taille',
                              '$prix',
                               '$annonceur_id',
                                '$pays',
                                 '$ville',
                                  '$equipement',
                                   '$type_chb',
                                   '$nb_chambre'
                          
                        )",
                $this->getTable()
            );

            var_dump($query);

            $sth = $this->db_cnx->prepare($query);
            if (!$sth) {
                return null;
            }
            // $sth->execute( [ 'id' => $id ]);

            // Attachement d'un paramètre avec précision de type
            //$sth->bindValue( 'id', $id, PDO::PARAM_INT );

            // Exécution de la requête préparée
            $sth->execute();

            $sth->debugDumpParams();

            // En cas d'erreur du serveur SQL on retourne null
            if ($sth->errorCode() !== PDO::ERR_NONE) {
                return null;
            }

            $row = $sth->fetch();
            if (!$row) {
                return null;
            }

            $_SESSION['row'] = $row;

        }

        var_dump($_SESSION['row']);
        return $_SESSION['row'];
    }





}