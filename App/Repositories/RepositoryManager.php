<?php

namespace App\Repositories;

use Core\Database;

class RepositoryManager
{
	private static ?self $instance = null;

	private UserRepository $user_repo;
	public function getUserRepo(): UserRepository
	{
		return $this->user_repo;
	}

	private CategoryRepository $category_repo;
	public function getCategoryRepo(): CategoryRepository
	{
		return $this->category_repo;
	}

	private PostRepository $post_repo;
	public function getPostRepo(): PostRepository
	{
		return $this->post_repo;
	}

    private AnnonceRepository $Annonce_repo;
    public function getAnnonceRepo(): AnnonceRepository
    {
        return $this->annonce_repo;
    }

    private FavorisRepository $favoris_repo;
    public function getFavorisRepo(): FavorisRepository
    {
        return $this->favoris_repo;
    }

    private ReservationRepository $reservation_repo;
    public function getReservationRepo(): ReservationRepository
    {
        return $this->reservation_repo;
    }

    private AnnulationRepository $annulation_repo;
    public function getAnnulationRepo(): AnnulationRepository
    {
        return $this->annulation_repo;
    }

    public static function getRm(): self
	{
		if( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct()
	{
		$pdo = Database::get();

		$this->user_repo = new UserRepository( $pdo );
		$this->category_repo = new CategoryRepository( $pdo );
		$this->post_repo = new PostRepository( $pdo );
		$this->annonce_repo = new AnnonceRepository( $pdo );
		$this->reservation_repo = new ReservationRepository( $pdo );
		$this->favoris_repo = new FavorisRepository( $pdo );
		$this->annulation_repo = new AnnulationRepository( $pdo );
	}

	private function __clone() { }
	private function __wakeup() { }
}