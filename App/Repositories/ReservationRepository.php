<?php

namespace App\Repositories;

use App\Models\Favoris;
use Core\Repository;
use App\Models\reservation;
use \PDO;


class ReservationRepository extends Repository
{
    public function getTable(): string
    {
        return 'reservation';
    }

    public function reservation(): ?Reservation
    {
        if ( isset( $_POST['reservation'] ) ) {
            $utilisateur_id = $_SESSION['id'];
            $chambre_id = $_POST['reservation'];
            $annonceur_id = $_POST['annonceur'];


            $queryAddReservation = sprintf(
                "INSERT INTO %s (`utilisateur_id`, `chambre_id`, `annonceur_id`) VALUES ( '$utilisateur_id', '$chambre_id', '$annonceur_id')",
                $this->getTable()
            );

            $sth = $this->db_cnx->prepare($queryAddReservation);
            if (!$sth) {
                return null;
            }

            $queryReadReservation = sprintf(
                'SELECT * FROM %s r JOIN utilisateur u ON r.utilisateur_id = u.id
                    JOIN chambre c ON c.id = r.chambre_id',
                $this->getTable()
            );

            $sth2 = $this->db_cnx->prepare($queryReadReservation);
            if (!$sth2) {
                return null;
            }


            // $sth->execute( [ 'id' => $id ]);

            // Attachement d'un paramètre avec précision de type
            //$sth->bindValue( 'id', $id, PDO::PARAM_INT );

            // Exécution de la requête préparée
            $sth->execute();

            $sth->debugDumpParams();

            // En cas d'erreur du serveur SQL on retourne null
            if ($sth->errorCode() !== PDO::ERR_NONE) {
                return null;
            }

            $row = $sth->fetch();
            if (!$row) {
                return null;
            }

            $_SESSION['row'] = $row;

        }

        return $_SESSION['row'];
    }

    public function readReservation(int $id): ?array
    {
        $result = [];
        $queryReadReservation = sprintf(
            'SELECT * FROM %s r JOIN utilisateur u ON r.utilisateur_id = u.id
                    JOIN chambre c ON c.id = r.chambre_id
                    WHERE r.utilisateur_id=:id',
            $this->getTable()
        );

        $sth = $this->db_cnx->prepare($queryReadReservation);
        if (!$sth) {
            return null;
        }


        // $sth->execute( [ 'id' => $id ]);

        // Attachement d'un paramètre avec précision de type
        $sth->bindValue( 'id', $id, PDO::PARAM_INT );

        // Exécution de la requête préparée
        $sth->execute();

//        $sth->debugDumpParams();

        // En cas d'erreur du serveur SQL on retourne null
        if ($sth->errorCode() !== PDO::ERR_NONE) {
            return null;
        }

        while ($row = $sth->fetch()) {
            $result[] =  $row ;
        }

        return $result;
    }

    public function readAnnonceurReservation(int $id): ?array
    {
        $result = [];
        $queryReadAnnonceurReservation = sprintf(
            'SELECT * FROM %s r JOIN utilisateur u ON r.utilisateur_id = u.id
                    JOIN chambre c ON c.id = r.chambre_id
                    WHERE r.annonceur_id=:id',
            $this->getTable()
        );

        $sth = $this->db_cnx->prepare($queryReadAnnonceurReservation);
        if (!$sth) {
            return null;
        }


        // $sth->execute( [ 'id' => $id ]);

        // Attachement d'un paramètre avec précision de type
        $sth->bindValue( 'id', $id, PDO::PARAM_INT );

        // Exécution de la requête préparée
        $sth->execute();

//        $sth->debugDumpParams();

        // En cas d'erreur du serveur SQL on retourne null
        if ($sth->errorCode() !== PDO::ERR_NONE) {
            return null;
        }

        while ($row = $sth->fetch()) {
            $result[] =  $row ;
        }

        return $result;
    }

    // Read: favoris
    public function findReservation (int $id ): ?array
    {
        return $this->readReservation( $id, Reservation::class );
    }




}