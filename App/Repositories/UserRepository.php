<?php

namespace App\Repositories;

use Core\Repository;
use App\Models\User;
use \PDO;


class UserRepository extends Repository
{
	public function getTable(): string
	{
		return 'utilisateur';
	}

	// CRUD
	// Read: Toute la liste
	public function findAll(): array
	{
		return $this->readAll( User::class );
	}

	// Read: Un User par son ID
	public function findById( int $id ): ?User
	{
		return $this->readById( $id, User::class );
	}

    // Read: Un User par son Login
    public function findByLogin( string $login ): ?User
    {
        return $this->readByLogin( $login, User::class );
    }

    public function inscription()
    {
        if ( isset( $_POST['btn-ins'] ) ) {
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $login = $_POST['login'];
            $password = $_POST['password'];
            $role = $_POST['role'];


            $query = sprintf(

                "INSERT INTO utilisateur(`nom`, `prenom`, `login`, `password`,`role`)
                        VALUES (
                        '$nom',
                        '$prenom',
                        '$login',
                        '$password',
                        '$role'
                        )",
                $this->getTable()
            );
            var_dump($query);

            $sth = $this->db_cnx->prepare( $query );
            if( !$sth ) {
                return null;
            }
            // $sth->execute( [ 'id' => $id ]);

            $sth->debugDumpParams();

            // Attachement d'un paramètre avec précision de type
//            $sth->bindValue( 'id', $id, PDO::PARAM_INT );

            // Exécution de la requête préparée
            $sth->execute();

            // En cas d'erreur du serveur SQL on retourne null
            if( $sth->errorCode() !== PDO::ERR_NONE ) {
                return null;
            }

            $row = $sth->fetch();
            if( !$row ) {
                return null;
            }

            /*
            // Pour débugguer
            $object = new $classname( $row );
            var_dump($object)
            return $object;
            */

            return new $classname( $row );
        }
    }






}