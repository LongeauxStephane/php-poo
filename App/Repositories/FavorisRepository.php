<?php

namespace App\Repositories;

use App\Models\Annonce;
use Core\Repository;
use App\Models\Favoris;

class FavorisRepository extends Repository
{
    public function getTable(): string
    {
        return 'favoris';
    }

    // Read: favoris
    public function findFavoris (int $id ): ?array
    {
        return $this->readFavoris( $id, Favoris::class );
    }

}