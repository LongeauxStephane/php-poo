<?php


namespace App\Models;


use Core\IModel;
use Core\Model;

class Equipement extends Model implements IModel
{
    //Informations sur les chambres
    public ?int $id;
    public ?string $titre;
    public ?string $prix;
    public ?string $taille;
    public ?string $description_courte;
    public ?string $description;
    public ?string $img;
    public ?string $equipement;
    //Type de chambre
    public ?int $chb_type;


    //Etat de la reservation -> Reservee ou non
    public ?string $reserv_etat;
    public ?string $reserv_date;

    //Var client et annonceur
    public ?string $utilisateur_id;
    public ?int $annonceur_id;

    //equipement
    public ?string $label;



}