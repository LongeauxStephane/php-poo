<?php


namespace App\Models;

use Core\IModel;
use Core\Model;



class Reservation extends Model implements IModel
{
    public ?int $utilisateur_id;
    public ?int $chambre_id;
}