<?php


namespace App\Models;

use Core\IModel;
use Core\Model;


class Annulation extends Model implements IModel
{
    //Informations sur les chambres
    public ?int $chambre_id;
    public ?int $utilisateur_id;
    public ?int $annonceur_id;

}