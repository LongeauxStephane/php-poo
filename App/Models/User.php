<?php

namespace App\Models;

use Core\IModel;
use Core\Model;


class User extends Model implements IModel
{
	public ?string $nom;
	public ?string $login;
	public ?string $password;
	public ?string $role;
}