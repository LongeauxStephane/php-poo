<?php

namespace Core;

class Router
{
	private static ?self $instance = null;

	// Liste des routes géré par l'application
	private array $_routes = [];

	public string $controllers_namespace = '\\';

	public static function get(): self
	{
		if( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Déclare une route
	 *
	 * @param string $uri URI de la route
	 * @param string $controller Nom du controller à utiliser
	 * @param string $action Nom de l'action (méthode du controller) à exécuter
	 */
	public function registerRoute( string $uri, string $controller, string $action ): void
	{
		$route = new Route( $uri, $controller, $action );
		$this->_routes[] = $route;
	}

	/**
	 * Démarrage du routeur
	 */
	public function start(): void
	{
		// URL demandée
		$request_uri = $_SERVER[ 'REQUEST_URI' ];

		// Flag URL trouvée
		$url_found = false;

		// Parcours des routes à la recherche de l'URL demandée
		foreach( $this->_routes as $route ) {
			// Tant que l'URL demandé ne correspond pas, on passe au tour suivant
			if( $route->uri !== $request_uri ) {
				continue;
			}

			$url_found = true;

			$controller_name = $this->controllers_namespace . '\\' . $route->controller;
			$action = $route->action;

			$controller = new $controller_name();
			$controller->$action();
			break;
		}

		if( !$url_found ) {
			$this->die404();
		}
	}

	public function die404(): void
	{
		http_response_code( 404 );
		$view = new View( 'error-404' );

		if( $view->getViewFileExists() ) {
			$view->render();

			return;
		}

		require_once ROOT_PATH . 'Core' . DS . 'ErrorViews' . DS . '404.php';
	}

    public function annonce(): void    {

        $view = new View( 'annonce' );

        if( $view->getViewFileExists() ) {
            $view->render();

            return;
        }

        require_once ROOT_PATH . 'App' . DS . 'Views' . DS . 'annonce.php';
    }

	private function __construct() {}
	private function __clone() {}
	private function __wakeup()	{}
}